package io.zencode.AOP_demo;

import io.zencode.AOP_demo.config.AOPDemoConfig;
import io.zencode.AOP_demo.dao.IAccountDAO;
import io.zencode.AOP_demo.entity.Account;
import io.zencode.AOP_demo.service.FortuneService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Client {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AOPDemoConfig.class);

        IAccountDAO accountDAO = context.getBean("accountDAO", IAccountDAO.class);
        FortuneService fortuneService = context.getBean("fortuneService", FortuneService.class);

        Account acc = new Account();
        acc.setAccountBalance(10000.0);
        acc.setAccountHolderName("Madhu");

        Account account = new Account("Tmoh", 1000000.0);

        accountDAO.addAccount(acc, false);
        accountDAO.addAccount(account, true);
        accountDAO.deleteAccount();

        accountDAO.setName("This is a name");
        accountDAO.setServiceCode(100);
        accountDAO.getServiceCode();
        accountDAO.getName();



        try {
            accountDAO.findAccounts();
            System.out.println(fortuneService.getFortune(true));
        } catch (Exception e) {
            System.out.println(e.getMessage());;
        }

        context.close();
    }
}
