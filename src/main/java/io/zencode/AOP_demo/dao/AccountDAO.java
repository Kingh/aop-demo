package io.zencode.AOP_demo.dao;


import io.zencode.AOP_demo.entity.Account;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccountDAO implements IAccountDAO {
    private String name;
    private Integer serviceCode;
    private List<Account> accounts;

    public String getName() {
        System.out.println("getName");
        return name;
    }

    public void setName(String name) {
        System.out.println("setName");
        this.name = name;
    }

    public Integer getServiceCode() {
        System.out.println("getServiceCode");
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        System.out.println("setServiceCode");
        this.serviceCode = serviceCode;
    }

    @Override
    public void addAccount(Account account, boolean vip) {
        System.out.println("DOING MY DB WORK: ADDING AN ACCOUNT");
        if (accounts == null) {
            accounts = new ArrayList<>();
        }

        accounts.add(account);
    }

    @Override
    public void deleteAccount() {
        System.out.println("DELETING ACCOUNT");
    }

    @Override
    public List<Account> findAccounts() throws Exception {
        if (accounts == null) {
            throw new Exception("Shit went down son");
        }

        return accounts;
    }


}
