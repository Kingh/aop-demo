package io.zencode.AOP_demo.dao;

import io.zencode.AOP_demo.entity.Account;

import java.util.List;

public interface IAccountDAO {
    void addAccount(Account account, boolean vip);
    void deleteAccount();
    List<Account> findAccounts() throws Exception;

    void setServiceCode(Integer serviceCode);
    Integer getServiceCode();
    String getName();
    void setName(String name);

}
