package io.zencode.AOP_demo.service;

import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class FortuneService {
    public String getFortune(boolean tripwire) throws Exception {
        if (tripwire) {
            throw new Exception("It all fell apart!!!");
        }

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "Hello is it me you are looking for";
    }
}
