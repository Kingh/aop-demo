package io.zencode.AOP_demo.aspect;

import io.zencode.AOP_demo.entity.Account;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Aspect
@Component
@Order(3)
public class LoggingDemoAspect {
    private static Logger logger = Logger.getLogger(LoggingDemoAspect.class.getName());

    @Before("io.zencode.AOP_demo.aspect.expression.AOPExpressions.forDAOPackageNoSetterGetter()")
    public void beforeAddAccountAdvice(JoinPoint joinPoint) {
        logger.info("====>>> Executing @Before advice method.... " + joinPoint.getSignature());
        Object[] args = joinPoint.getArgs();

        for (Object arg:
             args) {
            if (arg instanceof Account) {
                Account acc = (Account)arg;

                logger.info(acc.toString());
            } else {
                logger.info(arg.toString());
            }
        }
    }

    @AfterReturning(
            pointcut = "io.zencode.AOP_demo.aspect.expression.AOPExpressions.forFindAccounts()",
            returning = "result"
    )
    public void afterReturningFindAccountsAdvice(JoinPoint joinPoint, List<Account> result) {
        logger.info("Post processing.....");
        logger.info("====>>> Executing @AfterReturning on method: " + joinPoint.getSignature());

        convertAccountNamesToUpperCase(result);

        logger.info("====>>> result is: " + result);
    }

    @AfterThrowing(
            pointcut = "io.zencode.AOP_demo.aspect.expression.AOPExpressions.forFindAccounts()",
            throwing = "exception"
    )
    public void afterThrowingFindAccountsAdvice(JoinPoint joinPoint, Throwable exception) {
        logger.info("====> Executing @AfterThrowing on method: " + joinPoint.getSignature());

        logger.info(exception.toString());
    }

    @After("io.zencode.AOP_demo.aspect.expression.AOPExpressions.forFindAccounts()")
    public void afterFindAccountsAdvice(JoinPoint joinPoint) {
        logger.info("====>>> Executing @After (finally) on method: " + joinPoint.getSignature());
    }

    @Around("io.zencode.AOP_demo.aspect.expression.AOPExpressions.forGetFortune()" )
    public Object afterGetFortune(ProceedingJoinPoint joinPoint) throws Throwable {

        logger.info("====>>> Executing @Around advice on method: " + joinPoint.getSignature());
        long begin = System.currentTimeMillis();

        Object result;

        long end = System.currentTimeMillis();

        logger.info("====>>> Duration: " + (end - begin) / 1000.0 + " seconds");

        try {
            result = joinPoint.proceed();  // Execute target method
        } catch (Exception exc) {
            logger.info("@Around advice: Huston we have a problem " + exc);

            result = "Nothing to see here, move along";
        }

        return result;
    }

    private void convertAccountNamesToUpperCase(List<Account> result) {
        for (Account account: result) {
            account.setAccountHolderName(account.getAccountHolderName().toUpperCase());
        }
    }
}
