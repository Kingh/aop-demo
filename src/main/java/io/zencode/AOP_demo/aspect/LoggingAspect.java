package io.zencode.AOP_demo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

//@Aspect
//@Component
public class LoggingAspect {
    @Pointcut("execution(* io.zencode.AOP_demo.dao.*.*(..))")
    private void forDaoPackage() {}

    @Pointcut("execution(* io.zencode.AOP_demo.dao.*.set*(..))")
    private void setter() {}

    @Pointcut("execution(* io.zencode.AOP_demo.dao.*.get*(..))")
    private void getter() {}

    @Pointcut("forDaoPackage() && !(getter() || setter())")
    private void forDaoPackageNoGetterSetter() {}

    @Pointcut("getter() || setter()")
    private void forDaoPackageGetterSetter() {}

    @Before("forDaoPackageNoGetterSetter()")
    public void doaPackage() {
        System.out.println("\nfor DAO package no setter and getter");
    }

    @Before("forDaoPackageGetterSetter()")
    public void daoSetterGetter() {
        System.out.println("\nBefore setters and getters");
    }

    @Before("execution(public void *Account(..))") // --> Pointcut expression
//    @Before("execution(public void *Account(Account))") // --> Parameter name must be a fully qualified classname
//    @Before("execution(* io.zencode.AOP_demo.dao.AccountDAO.*())") // --> Pointcut expression
    public void beforeAddAccountAdvice() {
        System.out.println("\n======> Executing @Before advice on addAccount()");
    }
}
/********************************************************
 *  - Keep the code small                               *
 *  - Keep the code fast                                *
 *  - Do not perform any expensive / slow operations    *
 *  - Get in and out as QUICKLY as possible             *
 ********************************************************/