package io.zencode.AOP_demo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
@Order(1)
public class CloudLogAspect {
    private static Logger logger = Logger.getLogger(LoggingDemoAspect.class.getName());

    @Before("io.zencode.AOP_demo.aspect.expression.AOPExpressions.forDAOPackageNoSetterGetter()")
    public void logToCloudAdvice() {
        logger.info("\n====>>> Logging to cloud in async fashion...");
    }
}
