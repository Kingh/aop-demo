package io.zencode.AOP_demo.aspect.expression;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AOPExpressions {
    @Pointcut("execution(* io.zencode.AOP_demo.dao.*.*(..))")
    public void forDAOPackage() {}

    @Pointcut("execution(* io.zencode.AOP_demo.dao.AccountDAO.findAccounts(..))")
    public void forFindAccounts() {}

    @Pointcut("execution(* io.zencode.AOP_demo.dao.*.get*())")
    public void forGetter() {}

    @Pointcut("execution(* io.zencode.AOP_demo.dao.*.set*(..))")
    public void forSetter() {}

    @Pointcut("forDAOPackage() && !(forGetter() || forSetter())")
    public void forDAOPackageNoSetterGetter() {}

    @Pointcut("execution(* io.zencode.AOP_demo.service.*.getFortune(..))")
    public void forGetFortune() {}
}
